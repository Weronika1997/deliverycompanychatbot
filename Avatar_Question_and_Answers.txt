1. Q: How much does it cost to send a package to a parcel machine?
   A: All shipping prices are available on our website.

2. Q: How can I check the location of my shipment?
   A: You can check the location of your shipment by entering the package number in the "location" tab.

3. Q: What happens if I don't pick up the shipment from the parcel machine within 48 hours?
   A: The package will be forwarded to the nearest pick-up point and await pick-up there.

4. Q: What happens if I don't pick up the package from the pick up point?
   A: The package will be returned to the sender.

5. Q: Where's the parcel machine?
   A: You can check the location of the parcel machines on our map in the "parcel machines" tab.